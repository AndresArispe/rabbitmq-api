package com.milankas.training.persistance.repository;

import com.milankas.training.persistance.entity.LogStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface LogRepository extends JpaRepository <LogStatus, UUID> {

}
