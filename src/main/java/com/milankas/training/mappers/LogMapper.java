package com.milankas.training.mappers;

import com.milankas.training.dto.LogStatusDto;
import com.milankas.training.persistance.entity.LogStatus;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LogMapper {

    LogStatusDto LogStatusToDto(LogStatus logStatus);

    List<LogStatusDto> toLogStatusDtos(List<LogStatus> logStatuses);

    LogStatus toLogStatus(LogStatusDto logStatusDto);
}
