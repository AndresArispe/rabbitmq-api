package com.milankas.training.controllers;

import com.milankas.training.services.LogService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;

@RestController
public class LogController {

    private LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @GetMapping(value = "/v1/logs")
    public ResponseEntity<Object> getLogsByDates(
                                @RequestParam(value = "level", required = false, defaultValue = "") String level,
                                @RequestParam(value = "service", required = false, defaultValue = "") String service,
                                @RequestParam(value = "message", required = false, defaultValue = "") String message,
                                @RequestParam(value = "fromDate", required = false) String fromDate,
                                @RequestParam(value = "toDate", required = false) String toDate) {
        if (fromDate == null && toDate != null) {
            return ResponseEntity.badRequest().body("fromDate value it's need it");
        }
        else {
            if (fromDate != null && toDate ==  null) {
                try {
                    String[] fromDateArray = fromDate.split("-");
                    Date fromDateFormat = logService.getDate(fromDateArray);
                    Date toDateFormat = new Date();
                    return new ResponseEntity<>(logService.getLogs(level, service, "%"+message+"%", fromDateFormat, toDateFormat), HttpStatus.OK);
                }
                catch (Exception e) {
                    return new ResponseEntity<>(e.toString(), HttpStatus.BAD_REQUEST);
                }
            }
            else {
                if (fromDate == null && toDate == null) {
                    return new ResponseEntity<>(logService.getLogs(level, service, "%"+message+"%", null, null), HttpStatus.OK);
                }
                else {
                    try {
                        String[] fromDateArray = fromDate.split("-");
                        String[] toDateArray = toDate.split("-");
                        Date fromDateFormat = logService.getDate(fromDateArray);
                        Date toDateFormat = logService.getDate(toDateArray);
                        return new ResponseEntity<>(logService.getLogs(level, service, "%"+message+"%", fromDateFormat, toDateFormat), HttpStatus.OK);
                    }
                    catch (Exception e) {
                        return new ResponseEntity<>(e.toString(), HttpStatus.BAD_REQUEST);
                    }
                }
            }
        }
    }
}
