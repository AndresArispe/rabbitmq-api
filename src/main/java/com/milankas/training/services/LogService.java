package com.milankas.training.services;

import com.milankas.training.persistance.entity.LogStatus;
import com.milankas.training.persistance.repository.LogRepository;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import javax.persistence.*;
import java.util.*;

@Service
public class LogService {

    private EntityManager entityManager;
    private LogRepository logRepository;

    public LogService(LogRepository logRepository, EntityManager entityManager) {
        this.logRepository = logRepository;
        this.entityManager = entityManager;
    }

    public Date getDate(String[] date) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(date[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(date[1])-1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date[2]));
        cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(date[3]));
        cal.set(Calendar.MINUTE, Integer.parseInt(date[4]));
        cal.set(Calendar.SECOND, Integer.parseInt(date[5]));
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public List<LogStatus> getLogs(String level, String service, String message, Date fromDate, Date toDate) {

        Map<String, Object> paramaterMap = new HashMap<String, Object>();
        List<String> whereCause = new ArrayList<String>();

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("select lt from LogStatus lt");

        if (!level.equals("")){
            whereCause.add(" lt.level =:level ");
            paramaterMap.put("level", level);
        }
        if (!service.equals("")){
            whereCause.add(" lt.serviceName =:service ");
            paramaterMap.put("service", service);
        }
        if (!message.equals("")){
            whereCause.add(" lt.message like :message ");
            paramaterMap.put("message", message);
        }
        if (fromDate != null){
            whereCause.add(" lt.dateAndTime >=:fromDate ");
            paramaterMap.put("fromDate", fromDate);
        }
        if (toDate != null){
            whereCause.add(" lt.dateAndTime <=:toDate ");
            paramaterMap.put("toDate", toDate);
        }

        queryBuilder.append(" where" + StringUtils.join(whereCause, " and "));
        Query jpaQuery =  entityManager.createQuery(queryBuilder.toString());

        for(String key :paramaterMap.keySet()) {
            jpaQuery.setParameter(key, paramaterMap.get(key));
        }

        jpaQuery.setFirstResult(1);
        jpaQuery.setMaxResults(50);

        return  jpaQuery.getResultList();
    }
}
