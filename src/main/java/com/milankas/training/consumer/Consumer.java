package com.milankas.training.consumer;

import com.milankas.training.config.MessagingConfig;
import com.milankas.training.dto.LogStatusDto;
import com.milankas.training.mappers.LogMapper;
import com.milankas.training.persistance.entity.LogStatus;
import com.milankas.training.persistance.repository.LogRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    @Autowired
    private LogRepository logRepository;

    @Autowired
    private LogMapper logMapper;

    @RabbitListener(queues = MessagingConfig.QUEUE)
    public void consumeMessageFromQueue(LogStatusDto logStatusDto) {
        LogStatus logStatus = logMapper.toLogStatus(logStatusDto);
        logRepository.save(logStatus);
        System.out.println("Message recieved from queue and saved : " + logStatusDto);
    }
}
